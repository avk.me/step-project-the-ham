/* Во всех секциях табы (tab) связаны с соответствующим содержанием(item) через дата-атрибуты*/

//Служебные функции:
const changeTab = function (event, activeTabClass) {
    const activeTab = document.querySelector(`.${activeTabClass}`);
    activeTab.classList.remove(activeTabClass);
    event.target.classList.add(activeTabClass);
}

const changeItem = function (itemsClass, activeTabClass, activeItemClass, dataAttr) {
    const items = document.querySelectorAll(`.${itemsClass}`);
    const activeTab = document.getElementsByClassName(activeTabClass);

    items.forEach(item => {
        item.classList.remove(activeItemClass);
        if (item.getAttribute(dataAttr) === activeTab[0].getAttribute(dataAttr)) {
            item.classList.add(activeItemClass);
        }
    })
}

// Переключатель вкладок секции "Our Services":
function switchService() {
    const serviceTabsContainer = document.querySelector('.service-tabs-wrapper');

    serviceTabsContainer.addEventListener('click', function (event) {
        changeTab(event, 'js-active-service');
        changeItem('service-item-description', 'js-active-service', 'js-active-item', 'data-service');
    })
}

//Переключатель вкладок + добавление доп. картинок секции "Our Amazing Work":
function manageWorkItems() {
    const workSectionsContainer = document.querySelector('.our-work-tabs');

    workSectionsContainer.addEventListener('click', function (event) {
        changeTab(event, 'js-active-work');
        const visibleItems = document.querySelectorAll('.visible');
        if (event.target.innerHTML === "All") {
            visibleItems.forEach(visibleItem => {
                visibleItem.classList.add('js-active-item');
            })
        } else {
            changeItem('visible', 'js-active-work', 'js-active-item', 'data-work');
        }
    })

    const loadButton = document.querySelector('.load-button');
    const workItems = document.querySelectorAll('.work-item');

    loadButton.addEventListener('click', function (event) {
        workItems.forEach(workItem => {
            workItem.classList.add('visible');
            const currentActiveSection = document.querySelector('.js-active-work');
            if (currentActiveSection.innerHTML === "All") {
                workItem.classList.add('js-active-item');
            } else if (workItem.dataset.work === currentActiveSection.dataset.work) {
                workItem.classList.add('js-active-item');
            }
        })
        loadButton.remove();
    })
}

//карусель секции "What People Say About theHam":
function carousel() {
    const peopleTabs = document.querySelector('.people-tabs');
    const personImgs = document.querySelectorAll('.person-img');
    const toggleBackwards = document.querySelector('.js-toggle-backwards');
    const toggleForwards = document.querySelector('.js-toggle-forwards');

    peopleTabs.addEventListener('click', function (event) {
        for (let i = 0; i < personImgs.length; i++) {
            if (event.target === personImgs[i]) {
                changeTab(event, 'js-active-person');
                changeItem('people-info', 'js-active-person', 'js-active-item', 'data-person')
            }
        }
    })
    toggleBackwards.addEventListener('click', function (event) {
        for (let i = 1; i < personImgs.length; i++) {
            if (personImgs[i].classList.contains('js-active-person')) {
                personImgs[i].classList.remove('js-active-person');
                personImgs[i - 1].classList.add('js-active-person');
                changeItem('people-info', 'js-active-person', 'js-active-item', 'data-person');
            }
        }
    })
    toggleForwards.addEventListener('click', function (event) {
        for (let i = personImgs.length - 2; i >= 0; i--) {
            if (personImgs[i].classList.contains('js-active-person')) {
                personImgs[i].classList.remove('js-active-person');
                personImgs[i + 1].classList.add('js-active-person');
                changeItem('people-info', 'js-active-person', 'js-active-item', 'data-person')
            }
        }
    })
}


switchService();
manageWorkItems();
carousel();